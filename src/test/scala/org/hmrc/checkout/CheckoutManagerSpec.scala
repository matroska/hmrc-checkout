package org.hmrc.checkout

import org.scalatest.{FlatSpec, MustMatchers}

/**
  * Created by tobialoschiavo on 02/08/16.
  */
class CheckoutManagerSpec extends FlatSpec with MustMatchers {

  val checkoutManager = CheckoutManager

  "A CheckoutManager" should "return 1.45 when Apple, Apple, Orange and Apple are passed with a 2x1 discount applied" in {
    val result = checkoutManager.calculateTotal(
        Array("Apple", "Apple", "Orange", "Apple"))

    result must ===(BigDecimal(1.45))
  }

  it should "return 0.85 when Apple and Orange are passed" in {
    val result = checkoutManager.calculateTotal(Array("Apple", "Orange"))

    result must ===(BigDecimal(0.85))
  }

  it should "return 0.85 when Apple, Orange and unknown element are passed" in {
    val result =
      checkoutManager.calculateTotal(Array("Apple", "Orange", "Not found"))

    result must ===(BigDecimal(0.85))
  }

  it should "return 1.70 when Apple and Orange are passed with discount of 2x1 and 3x1" in {
    val result = checkoutManager.calculateTotal(
        Array("Apple", "Apple", "Orange", "Orange", "Apple", "Orange"))

    result must ===(BigDecimal(1.70))
  }

  it should "return 0 if list is empty" in {

    val result = checkoutManager.calculateTotal(Array())

    result must ===(BigDecimal(0))
  }
}
