package org.hmrc.checkout

import org.scalatest.{FlatSpec, MustMatchers}

/**
  * Created by tobialoschiavo on 02/08/16.
  */
class SetItemsCatalogSpec extends FlatSpec with MustMatchers{

  val setItemsCatalog = SetItemsCatalog

  "A SetItemsCatalog" should "return an Apple Item if called with 'Apple'" in {
    val Some(Item(name, price)) = setItemsCatalog.getProductByName("Apple")

    name must ===("Apple")
    price must ===(BigDecimal(0.60))
  }

  it should "return an Orange Item if called with 'Orange'" in {
    val Some(Item(name, price)) = setItemsCatalog.getProductByName("Orange")

    name must ===("Orange")
    price must ===(BigDecimal(0.25))
  }

  it should "return None if item is not found" in {
    val result = setItemsCatalog.getProductByName("Not found")

    result must ===(None)
  }
}
