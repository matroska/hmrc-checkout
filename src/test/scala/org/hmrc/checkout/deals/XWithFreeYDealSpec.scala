package org.hmrc.checkout.deals

import org.hmrc.checkout.Item
import org.scalatest.{FlatSpec, MustMatchers}

/**
  * Created by tobialoschiavo on 02/08/16.
  */
class XWithFreeYDealSpec extends FlatSpec with MustMatchers {
  val apple = Item("Apple", BigDecimal(0.6))
  val orange = Item("Orange", BigDecimal(0.25))

  "An XWithFreeYDealSpec" should "return the price by Y if X products are presents" in {
    val items = Seq(apple, apple, apple, orange)
    val xForYDeal = new XWithFreeYDeal("Apple", 2, 1)

    val discount = xForYDeal.computeDiscount(items)

    discount must ===(BigDecimal(0.6))
  }

  it should "return 2.4 if 4 items are free if six apples are purchased" in {
    val items = Seq(apple, apple, apple, apple, apple, apple, apple)
    val xForYDeal = new XWithFreeYDeal("Apple", 3, 2)

    val discount = xForYDeal.computeDiscount(items)

    discount must ===(BigDecimal(2.4))
  }

  it should "return 0 if 0 items are purchased" in {
    val items = Seq()
    val xForYDeal = new XWithFreeYDeal("Orange", 3, 1)

    val discount = xForYDeal.computeDiscount(items)

    discount must ===(BigDecimal(0))
  }

  it should "throw an exception if x is zero" in {
    intercept[IllegalArgumentException] {
      val xForYDeal = new XWithFreeYDeal("Orange", 0, 2)
    }
  }

  it should "throw an exception if y is zero" in {
    intercept[IllegalArgumentException] {
      val xForYDeal = new XWithFreeYDeal("Orange", 2, 0)
    }
  }
}
