package org.hmrc.checkout.deals

import org.hmrc.checkout.Item
import org.mockito.Mockito._
import org.scalatest.mock.MockitoSugar.mock
import org.scalatest.{FlatSpec, MustMatchers}

/**
  * Created by tobialoschiavo on 02/08/16.
  */
class SimpleDealEvaluatorSpec extends FlatSpec with MustMatchers {

  val firstDealMock = mock[Deal]
  val secondDealMock = mock[Deal]

  val items =
    Seq(Item("Apple", BigDecimal(0.6)), Item("Orange", BigDecimal(0.25)))

  "A SimpleDealEvaluator" should "call getDiscount on every deal passed and sum the results" in {
    val simpleDealEvaluator = new SimpleDealEvaluator(
      Seq(firstDealMock, secondDealMock))
    when(firstDealMock.computeDiscount(items)).thenReturn(BigDecimal(0.1))
    when(secondDealMock.computeDiscount(items)).thenReturn(BigDecimal(0.15))

    val result = simpleDealEvaluator.getDiscount(items)

    result must ===(BigDecimal(0.25))
  }

  it should "return 0 if deal list is empty" in {

    val result = new SimpleDealEvaluator(Seq()).getDiscount(items)

    result must ===(BigDecimal(0))
  }

}
