package org.hmrc.checkout

/**
  * Created by tobialoschiavo on 02/08/16.
  */
object SetItemsCatalog extends ItemsCatalog {

  val items =
    Set(Item("Apple", BigDecimal(0.60)), Item("Orange", BigDecimal(0.25)))

  val itemMap = items.map(el => el.name -> el).toMap

  def getProductByName(name: String): Option[Item] = itemMap.get(name)

}

trait ItemsCatalog {
  def getProductByName(name: String): Option[Item]
}

case class Item(name: String, price: BigDecimal)
