package org.hmrc.checkout.deals

import org.hmrc.checkout.Item

/**
  * Created by tobialoschiavo on 01/08/16.
  */
trait DealEvaluator {

  def getDiscount(items: Seq[Item]): BigDecimal
}

class SimpleDealEvaluator(deals: Seq[Deal]) extends DealEvaluator {

  def getDiscount(items: Seq[Item]): BigDecimal = {
    if (deals.isEmpty) BigDecimal(0)
    else deals.map(_.computeDiscount(items)).sum
  }

}
