package org.hmrc.checkout.deals

import org.hmrc.checkout.Item

/**
  * Created by tobialoschiavo on 02/08/16.
  */
trait Deal {

  def computeDiscount(items: Seq[Item]): BigDecimal

}

class XWithFreeYDeal(itemName: String,
                     x: Int,
                     y: Int)
    extends Deal {
  require(x != 0)
  require(y != 0)

  def computeDiscount(items: Seq[Item]): BigDecimal = {
    if (items.isEmpty) BigDecimal(0)
    else {
      val filtered = items.filter(_.name == itemName)
      val price = filtered.headOption.map(_.price).getOrElse(BigDecimal(0))
      price * (filtered.size / x * y)
    }
  }
}
