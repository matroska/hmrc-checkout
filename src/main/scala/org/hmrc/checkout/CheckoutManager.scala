package org.hmrc.checkout

import org.hmrc.checkout.deals.{SimpleDealEvaluator, XWithFreeYDeal}

/**
  * Created by tobialoschiavo on 02/08/16.
  */
object CheckoutManager {

  private[this] val dealEvaluator = new SimpleDealEvaluator(
      Seq(new XWithFreeYDeal("Apple", 2, 1),
          new XWithFreeYDeal("Orange", 3, 1)))
  private[this] val itemsCatalog: ItemsCatalog = SetItemsCatalog

  def calculateTotal(itemNames: Array[String]): BigDecimal = {
    val elements = itemNames.map(itemsCatalog.getProductByName).collect {
      case Some(el) => el
    }
    val totalCart = elements.foldLeft(BigDecimal(0)) { (total, el) =>
      total + el.price
    }
    val discount = dealEvaluator.getDiscount(elements)
    (totalCart - discount).max(BigDecimal(0))
  }
}
